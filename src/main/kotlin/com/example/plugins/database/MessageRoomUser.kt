package com.example.plugins.database

import com.example.plugins.database.UserService.Users
import kotlinx.coroutines.Dispatchers
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.transactions.transaction

@Serializable
data class MessageRoomUser(val id: Int?, val userId: Int, val roomId: Int)

class MessageRoomUserService(private val database: Database) {
    object MessageRoomUsers : Table() {
        val id = integer("id").autoIncrement()
        val userId = integer("user_id") references UserService.Users.id
        val roomId = integer("room_id") references MessageRoomService.MessageRooms.id

        override val primaryKey = PrimaryKey(MessageRoomUsers.id)
    }

    init {
        transaction(database) {
            SchemaUtils.create(MessageRoomUsers)
        }
    }

    suspend fun <T> dbQuery(block: suspend () -> T): T =
        newSuspendedTransaction(Dispatchers.IO) { block() }

    suspend fun findRoomUserByUserId(roomId: Int, userId: Int): MessageRoomUser? {
        return dbQuery {
            MessageRoomUsers.select { (MessageRoomUsers.userId eq userId) and (MessageRoomUsers.roomId eq roomId)}
                .map { MessageRoomUser(it[MessageRoomUsers.id], it[MessageRoomUsers.userId], it[MessageRoomUsers.roomId]) }.singleOrNull()
        }
    }
    suspend fun addUserToRoom(userId: Int, roomId: Int): MessageRoomUser {
        return dbQuery {
            // Check if the user-room pair already exists
            val existingRecord = MessageRoomUsers
                .select { (MessageRoomUsers.userId eq userId) and (MessageRoomUsers.roomId eq roomId) }
                .singleOrNull()

            if (existingRecord == null) {
                // Insert the new user-room pair
                val messageRoomUser = MessageRoomUsers.insert {
                    it[this.userId] = userId
                    it[this.roomId] = roomId
                }
                // Return the newly inserted record
                MessageRoomUser(messageRoomUser[MessageRoomUsers.id], messageRoomUser[MessageRoomUsers.userId], messageRoomUser[MessageRoomUsers.roomId])
            } else {
                // Return the existing record
                MessageRoomUser(existingRecord[MessageRoomUsers.id], existingRecord[MessageRoomUsers.userId], existingRecord[MessageRoomUsers.roomId])
            }
        }
    }

    suspend fun removeUserFromRoom(userId: Int, roomId: Int) {
        dbQuery {
            MessageRoomUsers.deleteWhere { (MessageRoomUsers.userId eq userId) and (MessageRoomUsers.roomId eq roomId) }
        }
    }
}