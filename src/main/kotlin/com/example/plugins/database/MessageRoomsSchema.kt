import com.example.plugins.database.MessageRoomUserService
import kotlinx.coroutines.Dispatchers
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.transactions.transaction

@Serializable
data class MessageRoom(val id: Int?, val name: String)

@Serializable
data class MessageRoomDetails(
    val messageRoom: MessageRoom,
    val messageRoomUser: List<OptionalMessageRoomUser>,
    val messages: List<OptionalMessage>
)

@Serializable
data class OptionalMessage(
    val id: Int?,
    val senderId: Int?,
    val messageRoomId: Int?,
    val content: String?,
    val timestamp: Long?
)

@Serializable
data class OptionalMessageRoomUser(
    val roomId: Int?,
    val userId: Int?,
    val id: Int?
)

class MessageRoomService(private val database: Database) {
    object MessageRooms : Table() {
        val id = integer("id").autoIncrement()
        val name = varchar("name", length = 100)

        override val primaryKey = PrimaryKey(id)
    }

    init {
        transaction(database) {
            SchemaUtils.create(MessageRooms)
        }
    }

    suspend fun <T> dbQuery(block: suspend () -> T): T =
        newSuspendedTransaction(Dispatchers.IO) { block() }

    suspend fun createMessageRoom(room: MessageRoom): Int = dbQuery {
        MessageRooms.insert {
            it[name] = room.name
        }[MessageRooms.id]
    }

    suspend fun readMessageRoom(id: Int): MessageRoom? {
        return dbQuery {
            MessageRooms.select { MessageRooms.id eq id }
                .map { MessageRoom(it[MessageRooms.id], it[MessageRooms.name]) }
                .singleOrNull()
        }
    }

    suspend fun getAllMessageRooms(currentUserId: Int): List<MessageRoomDetails> {
        return dbQuery {
                (MessageRooms leftJoin MessageRoomUserService.MessageRoomUsers leftJoin MessageService.Messages)
                    .slice(
                        MessageRooms.id,
                        MessageRooms.name,
                        MessageRoomUserService.MessageRoomUsers.userId,
                        MessageRoomUserService.MessageRoomUsers.roomId,
                        MessageRoomUserService.MessageRoomUsers.id,
                        MessageService.Messages.id,
                        MessageService.Messages.senderId,
                        MessageService.Messages.messageRoomId,
                        MessageService.Messages.content,
                        MessageService.Messages.timestamp
                    )
                    .select {
                        MessageRooms.id inSubQuery (
                                MessageRoomUserService.MessageRoomUsers
                                    .slice(MessageRoomUserService.MessageRoomUsers.roomId)
                                    .select { MessageRoomUserService.MessageRoomUsers.userId eq currentUserId }
                                )
                    }
                    .map {
                        val messageRoom = MessageRoom(it[MessageRooms.id], it[MessageRooms.name])
                        val messageRoomUser = OptionalMessageRoomUser(it[MessageRoomUserService.MessageRoomUsers.roomId], it[MessageRoomUserService.MessageRoomUsers.userId], it[MessageRoomUserService.MessageRoomUsers.id])
                        val message = OptionalMessage(it[MessageService.Messages.id], it[MessageService.Messages.senderId], it[MessageService.Messages.messageRoomId], it[MessageService.Messages.content], it[MessageService.Messages.timestamp])
                        Triple(messageRoom, messageRoomUser, message)
                    }
                    .groupBy { it.first }
                    .map { (messageRoom, group) ->
                        val users = group.map { it.second }.distinctBy { it.userId }.filter { it.id != null }
                        val messages = group.map { it.third }.filter { it.id != null }
                        MessageRoomDetails(messageRoom, users, messages)
                    }
        }
    }

    suspend fun getFullMessageRoomById(id: Int, currentUserId: Int): MessageRoomDetails? {
        return dbQuery {
            (MessageRooms leftJoin MessageRoomUserService.MessageRoomUsers leftJoin MessageService.Messages)
                .slice(
                    MessageRooms.id,
                    MessageRooms.name,
                    MessageRoomUserService.MessageRoomUsers.userId,
                    MessageRoomUserService.MessageRoomUsers.roomId,
                    MessageRoomUserService.MessageRoomUsers.id,
                    MessageService.Messages.id,
                    MessageService.Messages.senderId,
                    MessageService.Messages.messageRoomId,
                    MessageService.Messages.content,
                    MessageService.Messages.timestamp
                )
                .select {
                    MessageRooms.id inSubQuery (
                            MessageRoomUserService.MessageRoomUsers
                                .slice(MessageRoomUserService.MessageRoomUsers.roomId)
                                .select { (MessageRoomUserService.MessageRoomUsers.userId eq currentUserId) and (MessageRooms.id eq id) }
                            )
                }
                .map {
                    val messageRoom = MessageRoom(it[MessageRooms.id], it[MessageRooms.name])
                    val messageRoomUser = OptionalMessageRoomUser(it[MessageRoomUserService.MessageRoomUsers.roomId], it[MessageRoomUserService.MessageRoomUsers.userId], it[MessageRoomUserService.MessageRoomUsers.id])
                    val message = OptionalMessage(it[MessageService.Messages.id], it[MessageService.Messages.senderId], it[MessageService.Messages.messageRoomId], it[MessageService.Messages.content], it[MessageService.Messages.timestamp])
                    Triple(messageRoom, messageRoomUser, message)
                }
                .groupBy { it.first }
                .map { (messageRoom, group) ->
                    val users = group.map { it.second }.distinctBy { it.userId }.filter { it.id != null }
                    val messages = group.map { it.third }.filter { it.id != null }
                    MessageRoomDetails(messageRoom, users, messages)
                }.singleOrNull()
        }
    }

    suspend fun deleteMessageRoom(id: Int) {
        dbQuery {
            MessageRooms.deleteWhere { MessageRooms.id eq id }
        }
    }
}