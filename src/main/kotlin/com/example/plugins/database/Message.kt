import com.example.plugins.database.MessageRoomUser
import com.example.plugins.database.MessageRoomUserService
import com.example.plugins.database.UserService
import kotlinx.coroutines.Dispatchers
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.transactions.transaction

@Serializable
data class Message(val id: Int?, val senderId: Int, val messageRoomId: Int, val content: String, val timestamp: Long)

class MessageService(private val database: Database) {
    object Messages : Table() {
        val id = integer("id").autoIncrement()
        val senderId = integer("sender_id") references MessageRoomUserService.MessageRoomUsers.id
        val messageRoomId = integer("room_id") references MessageRoomService.MessageRooms.id
        val content = varchar("content", length = 1000)
        val timestamp = long("timestamp")

        override val primaryKey = PrimaryKey(id)
    }

    init {
        transaction(database) {
            SchemaUtils.create(Messages)
        }
    }

    suspend fun <T> dbQuery(block: suspend () -> T): T =
        newSuspendedTransaction(Dispatchers.IO) { block() }

    suspend fun createMessage(message: Message): Int = dbQuery {
        Messages.insert {
            it[senderId] = message.senderId
            it[messageRoomId] = message.messageRoomId
            it[content] = message.content
            it[timestamp] = message.timestamp
        }[Messages.id]
    }

    suspend fun readMessage(id: Int): Message? {
        return dbQuery {
            Messages.select { Messages.id eq id }
                .map { Message(it[Messages.id], it[Messages.senderId], it[Messages.messageRoomId], it[Messages.content], it[Messages.timestamp]) }
                .singleOrNull()
        }
    }

    suspend fun readMessagesByRoom(roomId: Int): List<Message> {
        return dbQuery {
            Messages.select { Messages.messageRoomId eq roomId }
                .map { Message(it[Messages.id], it[Messages.senderId], it[Messages.messageRoomId], it[Messages.content], it[Messages.timestamp]) }
        }
    }

    suspend fun deleteMessage(id: Int) {
        dbQuery {
            Messages.deleteWhere { Messages.id eq id }
        }
    }
}
