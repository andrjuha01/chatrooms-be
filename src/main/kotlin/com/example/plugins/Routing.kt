package com.example.plugins

import com.example.plugins.database.DatabaseConnection
import com.example.plugins.database.ExposedUser
import com.example.plugins.database.UserService
import com.example.routing.authenticationRoutes
import com.example.routing.messageRoomRoutes
import com.example.routing.websocketRoutes
import com.example.services.JwtService
import io.ktor.server.application.*
import io.ktor.server.plugins.autohead.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.http.*
import io.ktor.http.websocket.*
import io.ktor.server.request.*

fun Application.configureRouting(jwtService: JwtService) {
    install(AutoHeadResponse)
    val userService = UserService(DatabaseConnection.database)
    routing {
        get("/") {
            call.respondText("Hello World 1!")
        }
        // Create user
        post("/users") {
            val user = call.receive<ExposedUser>()
            val id = userService.create(user)
            call.respond(HttpStatusCode.Created, id)
        }

        // Read user
        get("/users/{id}") {
            val id = call.parameters["id"]?.toInt() ?: throw IllegalArgumentException("Invalid ID")
            val user = userService.read(id)
            if (user != null) {
                call.respond(HttpStatusCode.OK, user)
            } else {
                call.respond(HttpStatusCode.NotFound)
            }
        }

        // Update user
        put("/users/{id}") {
            val id = call.parameters["id"]?.toInt() ?: throw IllegalArgumentException("Invalid ID")
            val user = call.receive<ExposedUser>()
            userService.update(id, user)
            call.respond(HttpStatusCode.OK)
        }

        // Delete user
        delete("/users/{id}") {
            val id = call.parameters["id"]?.toInt() ?: throw IllegalArgumentException("Invalid ID")
            userService.delete(id)
            call.respond(HttpStatusCode.OK)
        }
    }

    authenticationRoutes()
    messageRoomRoutes()
    websocketRoutes(jwtService)
}
