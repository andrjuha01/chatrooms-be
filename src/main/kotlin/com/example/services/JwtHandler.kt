package com.example.services

import com.auth0.jwt.JWT
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm
import com.example.models.UserCredentials
import com.example.plugins.database.DatabaseConnection
import com.example.plugins.database.ExposedUser
import com.example.plugins.database.UserService
import io.ktor.server.application.*
import io.ktor.server.auth.jwt.*
import kotlinx.coroutines.*
import org.mindrot.jbcrypt.BCrypt
import java.util.*
class JwtService(
    private val application: Application,
) {
    private val secret = "secret"
    private val issuer = "http://0.0.0.0:8080/"
    private val audience = "http://0.0.0.0:8080/chatrooms"
    val realm =  "Access to 'chatrooms'"
    val db = DatabaseConnection.database
    val userService = UserService(db)
    val jwtVerifier: JWTVerifier =
        JWT
            .require(Algorithm.HMAC256(secret))
            .withAudience(audience)
            .withIssuer(issuer)
            .build()
    fun createJwtToken(loginRequest: UserCredentials): String? {
        val scope = CoroutineScope(Dispatchers.Default)
        val dataDeferred = scope.async {
            userService.findByUsername(loginRequest.username)
        }
        val foundUser: ExposedUser? = runBlocking {
            dataDeferred.await()
        }

        if (foundUser != null && BCrypt.checkpw(loginRequest.password, foundUser.password))
            return JWT.create()
                .withAudience(audience)
                .withIssuer(issuer)
                .withClaim("username", loginRequest.username)
                .withExpiresAt(Date(System.currentTimeMillis() + 3_600_000))
                .sign(Algorithm.HMAC256(secret))
        else
            return null
    }
    fun customValidator(
        credential: JWTCredential,
    ): JWTPrincipal? {
        val username: String = extractUsername(credential).toString()
        val scope = CoroutineScope(Dispatchers.Default)
        val dataDeferred = scope.async {
            userService.findByUsername(username)
        }
        val foundUser: ExposedUser? = runBlocking {
            dataDeferred.await()
        }

        return foundUser?.let {
            if (audienceMatches(credential))
                JWTPrincipal(credential.payload)
            else
                null
        }
    }
    private fun audienceMatches(
        credential: JWTCredential,
    ): Boolean =
        credential.payload.audience.contains(audience)
    private fun extractUsername(credential: JWTCredential): String? =
        credential.payload.getClaim("username").asString()
}