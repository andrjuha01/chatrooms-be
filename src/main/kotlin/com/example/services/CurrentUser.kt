package com.example.services

import com.example.plugins.database.DatabaseConnection
import com.example.plugins.database.ExposedUser
import com.example.plugins.database.UserService
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.auth.jwt.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CurrentUser private constructor(val user: ExposedUser) {
    companion object {
        suspend fun create(call: ApplicationCall): CurrentUser {
            val username = call.principal<JWTPrincipal>()!!.payload.getClaim("username").asString()
            val user = withContext(Dispatchers.IO) {
                UserService(DatabaseConnection.database).findByUsername(username)!!
            }
            return CurrentUser(user)
        }
    }
}