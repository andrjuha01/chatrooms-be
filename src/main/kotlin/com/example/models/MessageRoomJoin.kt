package com.example.models
import kotlinx.serialization.Serializable

@Serializable
data class MessageRoomJoin(val id: Int?)