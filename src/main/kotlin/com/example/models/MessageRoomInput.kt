package com.example.models
import kotlinx.serialization.Serializable

@Serializable
data class MessageRoomInput(val name: String)