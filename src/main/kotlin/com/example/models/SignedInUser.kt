package com.example.models

import kotlinx.serialization.Serializable

@Serializable
data class SignedInUser(val id: Int, val username: String, val jwt: String)