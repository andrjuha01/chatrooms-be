package com.example.routing

import MessageRoom
import MessageRoomService
import Message
import MessageService
import com.example.models.MessageRoomInput
import com.example.models.MessageRoomJoin
import com.example.plugins.database.DatabaseConnection
import com.example.plugins.database.MessageRoomUserService
import com.example.plugins.database.UserService
import com.example.services.CurrentUser
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Application.messageRoomRoutes() {
    val db = DatabaseConnection.database
    val userService = UserService(db)
    val messageRoomsService = MessageRoomService(db)
    val messageRoomUserService = MessageRoomUserService(db)
    val messageService = MessageService(db)

    routing {
        authenticate {
            get("message_rooms") {
                val currentUser = CurrentUser.create(call)

                val messageRoomDetailsList = messageRoomsService.getAllMessageRooms(currentUser.user.id!!)

                call.respond(HttpStatusCode.OK, messageRoomDetailsList)
            }

            get("message_rooms/{id}") {
                val currentUser = CurrentUser.create(call)
                val messageRoomId = call.parameters["id"]?.toIntOrNull()

                val messageRoomDetailsList = messageRoomsService.getFullMessageRoomById(messageRoomId!!, currentUser.user.id!!)
                if (messageRoomDetailsList == null) {
                    call.respond(HttpStatusCode.NotFound, "Message room not found")
                }
                call.respond(HttpStatusCode.OK, messageRoomDetailsList!!)
            }

            post("message_rooms") {
                val currentUser = CurrentUser.create(call)

                val name = call.receive<MessageRoomInput>().name
                if (name.isBlank()) {
                    call.respond(HttpStatusCode.BadRequest, "Name parameter is missing or empty")
                }

                // Cover in transaction
                val message_room_id = messageRoomsService.createMessageRoom(MessageRoom(null, name))
                messageRoomUserService.addUserToRoom(currentUser.user.id!!, message_room_id)
                val messageRoom = messageRoomsService.getFullMessageRoomById(message_room_id, currentUser.user.id!!)
                if (messageRoom != null) {
                    call.respond(HttpStatusCode.Created, messageRoom)
                } else {
                    call.respond(HttpStatusCode.NotFound, "Failed to create message room")
                }
            }

            post("message_rooms/join") {
                val currentUser = CurrentUser.create(call)
                val roomId = call.receive<MessageRoomJoin>().id;
                if (roomId == null) {
                    call.respond(HttpStatusCode.BadRequest, "Room ID parameter is missing or invalid")
                }

                val messageRoomId = messageRoomsService.readMessageRoom(roomId!!)
                if (messageRoomId == null) {
                    call.respond(HttpStatusCode.NotFound, "Message room not found")
                }

                messageRoomUserService.addUserToRoom(currentUser.user.id!!, roomId)
                val messageRoom = messageRoomsService.getFullMessageRoomById(roomId, currentUser.user.id)
                call.respond(HttpStatusCode.OK, messageRoom!!)
            }
        }

    }
}