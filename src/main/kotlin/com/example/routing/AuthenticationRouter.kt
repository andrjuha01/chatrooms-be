package com.example.routing

import com.example.errors.ErrorResponse
import com.example.models.SignedInUser
import com.example.models.UserCredentials
import com.example.plugins.database.DatabaseConnection
import com.example.plugins.database.ExposedUser
import com.example.plugins.database.UserService
import com.example.services.JwtService
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.auth.jwt.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.h2.jdbc.JdbcSQLIntegrityConstraintViolationException
import java.sql.SQLException


fun Application.authenticationRoutes() {
    val db = DatabaseConnection.database
    val userService = UserService(db)

    routing {
        post("users/sign_up") {
            val userCredentials = call.receive<UserCredentials>()
            if(!userCredentials.isValidCredentials()) {
                call.respond(HttpStatusCode.BadRequest, ErrorResponse("Username length should be >=3 and password length should be >= 6"))
            }
            val exposedUser = ExposedUser(id = null, userCredentials.username, userCredentials.hashedPassword())
            try {
                val id = userService.create(exposedUser)
                val jwt = JwtService(this.application).createJwtToken(userCredentials)
                call.respond(SignedInUser(id, exposedUser.username, jwt.toString()))
            } catch (e: SQLException) {
                if (e is JdbcSQLIntegrityConstraintViolationException) {
                    call.respond(HttpStatusCode.BadRequest, ErrorResponse("Username already exists, please try another one"))
                } else {
                    call.respond(HttpStatusCode.BadRequest, ErrorResponse("We were unable to process your request, please try again later"))
                }
            }
        }
        post("users/sign_in") {
            val userCredentials = call.receive<UserCredentials>()

            val exposedUser = userService.findByUsername(userCredentials.username)

            if (exposedUser != null) {
                val jwt = JwtService(this.application).createJwtToken(userCredentials)

                if(jwt == null) {
                    call.respond(HttpStatusCode.NotFound, ErrorResponse("User credentials are not valid"))
                }

                call.respond(SignedInUser(exposedUser.id!!, exposedUser.username, jwt!!))
            } else {
                call.respond(HttpStatusCode.NotFound, ErrorResponse("User credentials are not valid"))
            }
        }
        authenticate {
            get("users/secret") {
                call.respond(SignedInUser(7, extractPrincipalUsername(call).toString(), "astheus"))
            }
        }
    }
}

private fun extractPrincipalUsername(call: ApplicationCall): String? =
    call.principal<JWTPrincipal>()
        ?.payload
        ?.getClaim("username")
        ?.asString()