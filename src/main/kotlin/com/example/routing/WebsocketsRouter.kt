package com.example.routing

import Message
import MessageRoomDetails
import MessageRoomService
import MessageService
import com.auth0.jwt.JWT
import com.auth0.jwt.exceptions.JWTDecodeException
import com.auth0.jwt.interfaces.Claim
import com.example.plugins.database.DatabaseConnection
import com.example.plugins.database.MessageRoomUserService
import com.example.plugins.database.UserService
import com.example.services.CurrentUser
import com.example.services.JwtService
import io.ktor.server.websocket.*
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.jwt.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.websocket.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.withContext
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json


data class ConnectedUser(val userId: Int, val roomId: Int, val session: DefaultWebSocketSession)

val connectedUsers = mutableListOf<ConnectedUser>()

fun onConnect(userId: Int, roomId: Int, session: DefaultWebSocketSession) {
    connectedUsers.add(ConnectedUser(userId, roomId, session))
}

fun onDisconnect(userId: Int) {
    connectedUsers.removeIf { it.userId == userId }
}

suspend fun sendMessageToRoom(roomId: Int, messageRoom: MessageRoomDetails) {
    connectedUsers.filter { it.roomId == roomId }.forEach { user ->
        withContext(Dispatchers.IO) {
            user.session.send(Json.encodeToString(messageRoom))
        }
    }
}

fun Application.websocketRoutes(jwtService: JwtService) {
    val db = DatabaseConnection.database
    val userService = UserService(db)
    val messageRoomService = MessageRoomService(db)
    val messageService = MessageService(db)
    val messageRoomUserService = MessageRoomUserService(db)

    routing {
        webSocket("/connect/{roomId}") {
            // Extract JWT token from query parameters
            val token = call.request.queryParameters["token"]
            var roomIdParam = call.parameters["roomId"]

            if(roomIdParam == null){
                call.respond(HttpStatusCode.BadRequest, "Room ID parameter is missing or invalid")
            }

            val roomId = roomIdParam!!.toInt()

            // Validate JWT token
            if (token != null) {
                try {
                    val jwt = JWT.decode(token)
                    val jwtCredential = JWTCredential(jwt)
                    // Here, you can call your custom validation logic
                    val principal = jwtService.customValidator(jwtCredential)
                    if (principal != null) {
                        val username = principal.payload.getClaim("username").asString()
                        val user = withContext(Dispatchers.IO) {
                            UserService(DatabaseConnection.database).findByUsername(username)!!
                        }
                        onConnect(user.id!!, roomId, this)

                        try {
                            incoming.consumeEach { frame ->
                                if (frame is Frame.Text) {
                                    val text = frame.readText()
                                    val messageUserId = messageRoomUserService.findRoomUserByUserId(roomId, user.id)!!.id!!
                                    val message = Message(id=null, senderId=messageUserId, messageRoomId=roomId, content=text, timestamp=System.currentTimeMillis())
                                    messageService.createMessage(message)
                                    sendMessageToRoom(roomId, messageRoomService.getFullMessageRoomById(roomId, user.id)!!)
                                }
                            }
                        } finally {
                            onDisconnect(user.id)
                        }
                    } else {
                        call.respond(HttpStatusCode.BadRequest, "Invalid JWT token")
                    }
                } catch (e: JWTDecodeException) {
                    call.respond(HttpStatusCode.BadRequest, "Invalid JWT token")
                }
            } else {
                close(CloseReason(CloseReason.Codes.VIOLATED_POLICY, "Invalid token"))
            }
        }
    }
}