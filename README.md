Plan

- [x] Init the project
- [x] Add users table
- [x] Add registrations endpoint
- [x] Add login/logout(session management)
- [ ] Add ability to create chatroom
- [ ] Add ability to add users to chatroom
- [ ] Leave chatroom
- [ ] Websockets handling requests
- [ ] Access policies